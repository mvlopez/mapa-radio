import React, {useState} from "react";

function LoginForm({Login, error}){

const [details, setDetails] = useState({usuario:"", password:""});

const submitHandler = e =>{
    e.preventDefault();
    Login(details);
}


 return(
    <div className="flex justify-center content-center  ">
        <form className="bg-white shadow-md rounded px-8 pt-6 pb-8 mb-4" onSubmit={submitHandler}>
                <div className="mb-6">
                <p class="font-serif text-lg  text-gray-800 text-center font-bold">
                    LOGIN
                    </p>
                    { (error != "") ? ( <div className="text-red-500  text-center text-sm italic">{error}</div> ): "" }
                </div>
                <div className="mb-4">
                   
                    <div className="form-group">
                        <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="usuario">Usuario:</label>
                        <input type="text" className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" name="Usuario" id="Usuario" onChange={e => setDetails({...details, usuario: e.target.value})} value={details.usuario} ></input>
                    </div>
                    <div className="form-group">
                        <label className="block text-gray-700 text-sm font-bold mb-2" htmlFor="password">Password:</label>
                        <input type="password" className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" name="password" id="password" onChange={e => setDetails({...details, password: e.target.value})} value={details.password}  ></input>
                    </div>
                    
                </div>
                <div class="flex items-center justify-between">
                        <input className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline" type="submit" name="Login" />
                </div>
            </form>
    </div>
    
 )
}

export default LoginForm;
import React from "react";
export const FilterComponent = ({
  filterText,
  onFilter,
  onClear,
  onSearch,
}) => (
  <div className="flex flex-col flex-1">
    <div className="flex flex-1">
      <input
        id="search"
        type="text"
        placeholder="Buscar Usuario.. "
        aria-label="Search Input"
        value={filterText}
        onChange={onFilter}
        className="border-gray-300 px-4 py-2 rounded-l-lg flex-1"
      />
      <button
        type="button"
        onClick={() => onSearch()}
        className="text-white bg-sky-400 hover:bg-sky-600 p-3 rounded-r-lg"
      >
        <svg
          className="w-5 h-5"
          fill="none"
          stroke="currentColor"
          viewBox="0 0 24 24"
          xmlns="http://www.w3.org/2000/svg"
        >
          <path
            strokeLinecap="round"
            strokeLinejoin="round"
            strokeWidth="2"
            d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z"
          ></path>
        </svg>
        <span className="sr-only">BUSCAR</span>
      </button>
    </div>
    {filterText.trim() !== "" ? (
      <a
        href="#"
        className="justify-end underline underline-offset-1 pl-4 text-sm text-sky-500 hover:text-sky-600"
        onClick={onClear}
      >
        Limpiar Búsqueda
      </a>
    ) : (
      ""
    )}
  </div>
);

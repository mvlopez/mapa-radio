import React, { useState } from "react";
import { Link } from "react-router-dom";

import * as FaIcons from "react-icons/fa";
import * as AiIcons from "react-icons/ai";
import { SidebarData } from "./SidebarData.js";

import "./NavBar.css";
import { IconContext } from "react-icons";
/*
 <div className='welcome'>
      <h2> welcome <span>{user.name}</span> </h2>
      <button onClick={Logout}>Logout</button>
    </div>

    <div>
            <div className="w-24" >1</div>
            <div className="w-24">2</div>
            <div className="w-24">3</div>
        </div>

*/
function Navbar() {
  const [sidebar, setSidebar] = useState(false);
  const showSidebar = () => setSidebar(!sidebar);

  return (
    <>
      <nav className="bg-cyan-500 border-gray-200 px-2 sm:px-4 py-2.5 rounded dark:bg-gray-900">
        <div className="container flex flex-wrap justify-between items-center mx-auto">
          <span className="self-center text-xl font-semibold whitespace-nowrap dark:text-white">
            MAPA RADIO BASES
          </span>

          <div className="bg-cyan-500 hidden justify-between items-center w-full md:flex md:w-auto md:order-1">
            <ul className="flex flex-col p-4 mt-4 bg-cyan-500 rounded-lg border border-gray-100 md:flex-row md:space-x-8 md:mt-0 md:text-sm md:font-medium md:border-0  dark:bg-gray-800 md:dark:bg-gray-900 dark:border-gray-700">
              {SidebarData.map((item, Index) => {
                return (
                  <li
                    key={Index}
                    className="text-gray-500 dark:text-gray-400 hover:text-blue-600 dark:hover:text-blue-500"
                    aria-current="page"
                  >
                    <Link to={item.path}>
                      {item.icon}
                      <span> {item.title}</span>
                    </Link>
                  </li>
                );
              })}
            </ul>
          </div>
        </div>
      </nav>
    </>
  );
}

export default Navbar;

/*
      <div  class="grid grid-cols-6 bg-sky-400  ">
            <div className="flex justify-between items-center col-span-1 border-b-2 border-gray-100 py-6 md:justify-start  text-white  hover:bg-sky-300 ">
            <IconContext.Provider value={{ color: '#fff' }}>
                <div className="navbar">          
                    <Link to="#" className="menu-bar">
                    <FaIcons.FaBars onClick={showSidebar} /> 
                    </Link>
                  
                </div>
                <nav className={sidebar ? 'nav-menu active' : 'nav-menu'}>
                    <ul className='nav-menu-items' onClick={showSidebar}>
                        <li className="navbar-toggle" >
                            <Link to="#" className='menu-bars'>
                                <AiIcons.AiOutlineClose/>
                                
                            </Link>
                        </li>
                        {SidebarData.map((item,Index) => {
                            return (
                                <li Key={Index} className={item.cName}>
                                    <Link to={item.path}>
                                    {item.icon}
                                <span>     {item.title}</span>
                                    </Link>
                                </li>
                            )
                        })}
                    </ul>
                </nav>
                </IconContext.Provider>
            </div>
            <div  className=" flex justify-center content-center items-center col-span-4 px-4 border-b-2 border-gray-100 py-6 md:justify-start md:space-x-10 text-white  hover:bg-sky-300"> 
                      <h1 className="flex justify-center content-center text-3xl font-bold "> MAPA  RADIO </h1>                      
            </div>             
            
            
        </div>
*/

/*
 /* <div  className="bg-sky-400 flex-initial text-gray-700 text-right  px-4 py-2 m-2"> 
                    <button className="flex-shrink-0 bg-teal-500 hover:bg-teal-700 border-teal-500 hover:border-teal-700 text-sm border-4 text-white py-1 px-2 rounded" type="button">
                        Logout
                    </button>
                    </div>
*/

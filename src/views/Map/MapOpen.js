import React from "react";
import ReactDOM from "react-dom";
import { compose, withProps } from "recompose";
import {
  withScriptjs,
  withGoogleMap,
  GoogleMap,
   KmlLayer
} from "react-google-maps";


const MapWithAKmlLayer = compose(
  withProps({
    googleMapURL: "https://maps.googleapis.com/maps/api/js?key=AIzaSyCWNi_7AMSaWpXzLJkZE9yGXlW94WpTIYU&v=3.exp&libraries=geometry,drawing,places",
    loadingElement: <div style={{ height: `100%` }} />,
    containerElement: <div style={{ height: `400px` }} />,
    mapElement: <div style={{ height: `100%` }} />,
  }),
  withScriptjs,
  withGoogleMap
)(props =>
  <GoogleMap
    defaultZoom={13}
    defaultCenter={{ lat: 13.67463, lng: -89.20621 }}
  >
    <KmlLayer
      url="https://raw.githubusercontent.com/osmanlenin/java-app/master/localhost_8080_v1_api_kml_TEST2.kml"
      options={{ preserveViewport: true }}
    />
  </GoogleMap>
);



ReactDOM.render(<MapWithAKmlLayer  />, document.getElementById("root"));


export default function MapKml() {

  return (
    <>
   
    </>
  );
}
import React, { useContext, useState, useEffect, useCallback } from "react";
import MapKml from "./MapKml";
import { Authorize } from "../../context";
import { GoogleMap, useJsApiLoader, KmlLayer } from "@react-google-maps/api";

const containerStyle = {
  width: "100vw",
  height: "100vh",
};

const center = {
  lat: 13.67463,
  lng: -89.20621,
};

function Map() {
  const auth = useContext(Authorize);

  const { REACT_APP_BASE_URL, REACT_APP_KML_PATH } = process.env;
  const { user } = auth || {};
  const { usuario: username = "" } = user || {};
  const APIUrl = `${REACT_APP_BASE_URL}${REACT_APP_KML_PATH}${username}`;
  const [map, setMap] = useState(null);

  const { isLoaded } = useJsApiLoader({
    id: "google-map-script",
    googleMapsApiKey: "AIzaSyB6NLU-6f5psg-5Mc6LXQrlAyyR5LNG0C0",
  });
  const onLoad = useCallback(function callback(map) {
    const bounds = new window.google.maps.LatLngBounds(center);
    map.fitBounds(bounds);
    setMap(map);
  }, []);
  const onUnmount = useCallback(function callback(map) {
    setMap(null);
  }, []);
  return isLoaded ? (
    <GoogleMap
      mapContainerStyle={containerStyle}
      center={center}
      zoom={14}
      onLoad={onLoad}
      onUnmount={onUnmount}
    >
      {/* Child components, such as markers, info windows, etc. */}
      <KmlLayer url="https://raw.githubusercontent.com/osmanlenin/java-app/master/localhost_8080_v1_api_kml_TEST2.kml" />
    </GoogleMap>
  ) : (
    <div className="map-loading">Loading...</div>
  );
  // return <MapKml statement={APIUrl} />;
}
export default React.memo(Map);
/*
13.794185°, -88.89653°
const MapWithAKmlLayer = compose(
  withProps({
    googleMapURL: "https://maps.googleapis.com/maps/api/js?key=AIzaSyCWNi_7AMSaWpXzLJkZE9yGXlW94WpTIYU&v=3.exp&libraries=geometry,drawing,places",
    loadingElement: <div style={{ height: `100%` }} />,
    containerElement: <div style={{ height: `400px` }} />,
    mapElement: <div style={{ height: `100%` }} />,
  }),
  withScriptjs,
  withGoogleMap
)(props =>
  <GoogleMap
    defaultZoom={9}
    defaultCenter={{ lat: 41.9, lng: -87.624 }}
  >
    <KmlLayer
      url="http://googlemaps.github.io/js-v2-samples/ggeoxml/cta.kml"
      options={{ preserveViewport: true }}
    />
  </GoogleMap>
);

<MapWithAKmlLayer />
*/

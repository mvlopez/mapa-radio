import React, { useContext, useState, useEffect, useCallback } from "react";
// import MapKml from "./MapKml";
import { Authorize } from "../../context";
import { GoogleMap, useJsApiLoader, KmlLayer } from "@react-google-maps/api";
import {v4 as uuidv4} from 'uuid';

const containerStyle = {
  width: "100vw",
  height: "100vh",
};

const center = {
  lat: 13.67463,
  lng: -89.20621,
  // lat: 18.493078,
  // lng: -34.095021,
};

function Map() {
  const auth = useContext(Authorize);

  const { REACT_APP_BASE_URL, REACT_APP_KML_PATH,REACT_APP_KML_MOD } = process.env;
  const { user } = auth || {};
  const { usuario: username = "" } = user || {};
  const APIUrl = `${REACT_APP_BASE_URL}${REACT_APP_KML_PATH}${username}`;
  const [map, setMap] = useState(null);

  const { isLoaded } = useJsApiLoader({
    id: "google-map-script",
    googleMapsApiKey: "AIzaSyB6NLU-6f5psg-5Mc6LXQrlAyyR5LNG0C0",
  });
  
  let myuuid = uuidv4();    
  //console.log('Your UUID API KEY - : ' + "AIzaSyB6NLU-6f5psg-5Mc6LXQrlAyyR5LNG0C0");
  
  /*********FECHA MODIFICACION ***** */
  const [dateUpdate, setdateUpdate] = useState(null);

  const fechaModificacion = () => {
    
    const url = `${REACT_APP_BASE_URL}${REACT_APP_KML_MOD}`;

    fetch(url)
      .then((response) => {
        response.json().then((data) => {         
          setdateUpdate(data);    
          //console.log(data);      
        });
      })
      .catch((e) => {
        console.error(e);
      });
  };

  
  useEffect(() => {
      fechaModificacion();    
  }, []);

 ///console.log( "date update" +  {dateUpdate.name} );
//*********CARGA DEL MAPA************/

  const onLoad = useCallback(function callback(map) {
  //  console.log(`https://fullstack.telefonica-ca.movistar.com.sv/KmlService/v1/api/kml/${username}?${myuuid}`);

    const bounds = new window.google.maps.LatLngBounds(center);
  
    map.fitBounds(bounds);
    console.log( `https://fullstack.telefonica-ca.movistar.com.sv/KmlService/v1/api/kml/RB.FS.USER3?${myuuid}`);
    var georssLayer = new window.google.maps.KmlLayer({
      url: `https://fullstack.telefonica-ca.movistar.com.sv/KmlService/v1/api/kml/RB.FS.USER3?${myuuid}`,     
     // url: `https://fullstack.telefonica-ca.movistar.com.sv/KmlService/v1/api/kml/${username}?${myuuid}`,
      //url: `https://raw.githubusercontent.com/osmanlenin/java-app/master/MAR01.kml`,
      // url: `https://raw.githubusercontent.com/osmanlenin/java-app/master/localhost_8080_v1_api_kml_TEST2.kml`,
    });
    georssLayer.setMap(map);
  }, []);
 

  const onUnmount = useCallback(function callback(map) {
    setMap(null);
  }, []);

  useEffect(() => {
    if (map) {
      const zoom = map.getZoom();
      console.log({ map, zoom });

      // if (zoom > 19) {
      //   map.setZoom(14);
      // }
    }
  }, [map]);

  const onBoundsChanged = (e) => {
    if (map) {
      const zoom = map.getZoom();
      console.log({ map, zoom });

      // if (zoom > 19) {
      //   map.setZoom(19);
      // }
    }
  };
// <h1>Fecha de Modificacion: </h1> {dateUpdate.loadedDate}
  return isLoaded ? (
    <>
     <div className=" text-center">
     {dateUpdate &&
    dateUpdate.map((dateUpdate) => (
     <>
      Ultimo Archivo:  {dateUpdate.name}  
       </>
   ))}
    
      </div>
    <div className="map-loading">

    <GoogleMap
      mapContainerStyle={containerStyle}
      center={center}
      zoom={14}
      onLoad={onLoad}
      onBoundsChanged={onBoundsChanged}
      onUnmount={onUnmount}
    >
      {/* Child components, such as markers, info windows, etc. */}
      {/* <KmlLayer
        zIndex={10000}
        // url="http://www.google.com/maps/d/u/2/kml?forcekml=1&mid=12FaBuxEB7fv-UFcc346HBmX9Jwz3mmau"
        url={`http://raw.githubusercontent.com/osmanlenin/java-app/master/localhost_8080_v1_api_kml_TEST2.kml?version=${Date.now()}`}
        // url="http://api.flickr.com/services/feeds/geo/?g=322338@N20&lang=en-us&format=feed-georss"
      /> */}
    </GoogleMap>
    </div>
    
    </>
   
    
  ) : (
    <div className="map-loading">Loading...</div>
  );
  // return <MapKml statement={APIUrl} />;
}
export default React.memo(Map);
/*
13.794185°, -88.89653°
const MapWithAKmlLayer = compose(
  withProps({
    googleMapURL: "https://maps.googleapis.com/maps/api/js?key=AIzaSyCWNi_7AMSaWpXzLJkZE9yGXlW94WpTIYU&v=3.exp&libraries=geometry,drawing,places",
    loadingElement: <div style={{ height: `100%` }} />,
    containerElement: <div style={{ height: `400px` }} />,
    mapElement: <div style={{ height: `100%` }} />,
  }),
  withScriptjs,
  withGoogleMap
)(props =>
  <GoogleMap
    defaultZoom={9}
    defaultCenter={{ lat: 41.9, lng: -87.624 }}
  >
    <KmlLayer
      url="http://googlemaps.github.io/js-v2-samples/ggeoxml/cta.kml"
      options={{ preserveViewport: true }}
    />
  </GoogleMap>
);

<MapWithAKmlLayer />
*/

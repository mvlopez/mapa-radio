import React, { Component } from "react";
//import ReactDOM from "react-dom";
//mport { compose, withProps } from "recompose";
import {
  withScriptjs,
  withGoogleMap,
  GoogleMap,
   KmlLayer
} from "react-google-maps";

class MapKml extends Component{
    render() {
    const wsKml = this.props.statement;
  
    const GoogleMapExample = withGoogleMap(props => (
        <GoogleMap
            defaultZoom={14}
            defaultCenter={{ lat: 13.67463, lng: -89.20621 }}
        >
            <KmlLayer
                url="https://fullstack.telefonica-ca.movistar.com.sv/KmlService/v1/api/kml/TEST01"
                options={{ preserveViewport: true }}
            />
        </GoogleMap>
    ));


    return(
       <div>
         
         <GoogleMapExample            
           containerElement={ <div style={{ height: `500px`, width: '100%' }} /> }
           mapElement={ <div style={{ height: `100%` }} /> }
         />
       </div>
    );
    }
 };
 export default MapKml;

  // ReactDOM.render(<MapWithAKmlLayer  />, document.getElementById("root"));

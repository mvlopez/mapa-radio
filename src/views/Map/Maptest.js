import React from "react";
import { ReactSession } from 'react-client-session';
import MapKml from "./MapKml";

export default function MapTest() {


  const { REACT_APP_BASE_URL, REACT_APP_KML_PATH } = process.env;  
  const username = ReactSession.get("usuario");
  const APIUrl = `${REACT_APP_BASE_URL}${REACT_APP_KML_PATH}${username}`;
 /*
   <Helmet>
           <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCWNi_7AMSaWpXzLJkZE9yGXlW94WpTIYU"></script>
      </Helmet> 
      
        <MapKml/>
 */
  return (
    <>      
     <MapKml statement={APIUrl} />      
    </>
    );
   

}

/*
13.794185°, -88.89653°
const MapWithAKmlLayer = compose(
  withProps({
    googleMapURL: "https://maps.googleapis.com/maps/api/js?key=AIzaSyCWNi_7AMSaWpXzLJkZE9yGXlW94WpTIYU&v=3.exp&libraries=geometry,drawing,places",
    loadingElement: <div style={{ height: `100%` }} />,
    containerElement: <div style={{ height: `400px` }} />,
    mapElement: <div style={{ height: `100%` }} />,
  }),
  withScriptjs,
  withGoogleMap
)(props =>
  <GoogleMap
    defaultZoom={9}
    defaultCenter={{ lat: 41.9, lng: -87.624 }}
  >
    <KmlLayer
      url="http://googlemaps.github.io/js-v2-samples/ggeoxml/cta.kml"
      options={{ preserveViewport: true }}
    />
  </GoogleMap>
);

<MapWithAKmlLayer />
*/
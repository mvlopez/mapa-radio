import React, {
  useEffect,
  useState,
  useImperativeHandle,
  forwardRef,
} from "react";
import { useForm } from "react-hook-form";
import { Label, Textarea, TextInput } from "flowbite-react";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import uniq from "lodash/uniq";

const { REACT_APP_BASE_URL, REACT_APP_USER_ADD_PATH, REACT_APP_KML_ZC } = process.env;
const APIUrl = `${REACT_APP_BASE_URL}${REACT_APP_USER_ADD_PATH}`;

const reqOptions = {
  method: "POST",
  headers: { "Content-Type": "application/json" },
  body: {
    userId: {
      placemark: { id: null },
      userId: null,
    },
  },
};

const AddCellSchema = yup.object().shape({
  userId: yup.string().trim().required("Id Usuairo es requerido."),
  cellId: yup
    .string()
    .trim()
    .required("Debe seleccionar Zona Comercial.")
   /* .matches(
      /^(\s)?([A-Z]\d+(\s)?)(,(\s)?[A-Z]\d+(\s)?)*$/,
      "El formato esta equivocado, los Cell Id deben ser separados por comas. "
    ),*/
});

// eslint-disable-next-line import/no-anonymous-default-export
export const AddForm = forwardRef(({ onSave }, ref) => {
  const {
    register,
    handleSubmit,
    reset,
    formState: { errors },
  } = useForm({ resolver: yupResolver(AddCellSchema) });
  const [results, setResults] = useState([]);

  useImperativeHandle(ref, () => ({
    cleanForm() {
      reset();
      setResults([]);
    },
  }));
  // useEffect(() => {
  //   console.log({ props });
  // });

  const onSubmit = ({ userId, cellId }) => {
    
    const cells =
      cellId.indexOf(",") !== -1
        ? uniq(cellId.replace(/\s+/g, "").split(","))
        : [cellId.trim()];

    const promises = [];
    for (let i = 0; i < cells.length; i++) {
      const cell = cells[i];
      //console.log("cell " + cell);
      const options = {
        ...reqOptions,
        // body: JSON.stringify({ userId: { userId, placemark: { id: cell } } }),
         // body: JSON.stringify({ userId: { userId: userId, zoneName: {cell} } }),
        // body: { userId: { userId}, zoneName: {cell} },
        body: JSON.stringify({}),
      };
      //, options
      //promises.push(fetch(`${APIUrl}${userId}/${cell}`)); 
    //  console.log(options);
      promises.push(fetch(`${APIUrl}${userId}/${cell}`, options));
    }

    
    Promise.all(promises)

      .then((rs) => {
        const responses = [];

        rs.map((res, index) => {
          //console.log({res, index});
          if (res.status === 200 && res.ok === true) {
            responses.push({
              color: "text-green-500",
              message: `${index + 1}. Zona Comercial ${cells[index]} ha sido agregado`,
            });
          } else {
            if (res.status === 405) {
              responses.push({
                color: "text-orange-500",
                message: `Zona Comercial ${cells[index]} ya existe, code [${res.status}]`,
              });
            } else {
              responses.push({
                color: "text-orange-500",
                message: `Zona Comercial ${cells[index]} ha ocurrido un error, code [${res.status}]`,
              });
            }
          }
        });
        setResults(responses);
        onSave();
      })
      .catch((err) => console.log(err))
      .finally(() => reset());
  };

  /********* LLENAR COMBO ZONA COMERCIAL ******** */

  const [cbZonaComercial, setcbZonaComercial] = useState(null);

  const zonaComercialApi = () => {
    
    const urlZC = `${REACT_APP_BASE_URL}${REACT_APP_KML_ZC}`;

    fetch(urlZC)
      .then((response) => {
        response.json().then((data) => {         
          setcbZonaComercial(data);    
          //console.log(data);      
        });
      })
      .catch((e) => {
        console.error(e);
      });
  };

  
  useEffect(() => {
    zonaComercialApi();    
  }, []);


  /********************* */
  return (
    <form onSubmit={handleSubmit(onSubmit)} className="flex flex-col gap-4">
      <div>
        <div className="mb-2 block">
          <Label htmlFor="userId" value="User ID" />
        </div>
        <TextInput
          type="text"
          placeholder="ex: RD."
          {...register("userId")}
        />
        {errors.userId && (
          <p className="mt-2 text-sm text-red-600 dark:text-red-500">
            {errors.userId.message || "ha ocurrido un error"}
            {"!"}
          </p>
        )}
      </div>
     
      <div id="textarea">
      


      <div className="mb-2 block">
          <Label htmlFor="cellId" value="Zona Comercial" />
        </div>

        <select id="cellId"  {...register("cellId")}
        className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500">
            <option value="" selected>Seleccione...</option>
            {cbZonaComercial &&
                  cbZonaComercial.map((cbZonaComercial) => (
                  <>
                     <option value={cbZonaComercial}>{cbZonaComercial}  </option> 
                    </>
                ))}
                         
            </select>

       
        {errors.cellId && (
          <p className="mt-2 text-sm text-red-600 dark:text-red-500">
            {errors.cellId.message || "ha ocurrido un error"}
            {"!"}
          </p>
        )}

    
      </div>
      {results && (
        <div
          className={
            "flex flex-row overflow-y-auto" + results.length > 0 && "h-8"
          }
        >
          {results.map((result) => (
            <div className={result.color}>{result.message}</div>
          ))}
        </div>
      )}
      <div className="flex justify-center">
        <button
          type="submit"
          className="rounded text-white bg-sky-600/100 px-5 py-1 hover:bg-sky-700"
        >
          Save
        </button>
      </div>
    </form>
  );
});

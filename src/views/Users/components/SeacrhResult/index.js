import React, { useState } from "react";


export default function SearchResult({ results }){
    return(
        
        <div>
            <table class="w-full text-sm text-left text-gray-500 dark:text-gray-400">
                <thead class="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
                    <tr>
                        <th scope="col" class="py-3 px-6">
                            USUARIO
                        </th>
                        <th scope="col" class="py-3 px-6">
                            CELLID
                        </th>               
                        <th scope="col" class="py-3 px-6">
                            Action
                        </th>
                    </tr>
                </thead>
                <tbody>
                {
                    results?.map((value) => {
                        console.log({value});
                        return(
                    
                            <tr key={value.userId}  class="bg-white border-b dark:bg-gray-800 dark:border-gray-700">
                                <th scope="row" class="py-4 px-6 font-medium text-gray-900 whitespace-nowrap dark:text-white">
                                  {value.userId.userId}
                                </th>
                                <td class="py-4 px-6">
                                  {value.userId.placemark.id}
                                </td>                              
                                <td class="py-4 px-6">
                                    <a href="#" class="font-medium text-blue-600 dark:text-blue-500 hover:underline">Edit</a>
                                </td>
                            </tr>

                        

                        );
                    })}
                    
                    
                </tbody>
            </table>
            
        </div>
    );
}

/*
 
*/
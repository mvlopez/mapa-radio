import React, { useState } from "react";

/*value={selectedFile}
          onChange={(e) => setSelectedFile(e.target.files[0])} */

function AddUser(){

    

    const [userId,setUserId] = useState("");
    const [cellId, setCellId] = useState("");
    const [msgResult, setMsgResult] = useState("");
    
    const addUserID = (e) => setUserId(e.target.value);
    const addCellID = (e) => setCellId(e.target.value);    
    const enviarDatos = (e) => {
        e.preventDefault();
        console.log(`Usuaro : ${userId} cellId: ${cellId}`);
       
        if(!userId || userId == null || userId.trim() ==="" ){ 
            setMsgResult("El campo Usuario es requerido.")
        }else if(!cellId || cellId == null || cellId.trim() ==="" ){ 
            setMsgResult("El campo CellId es requerido.")
        }  else{
            const requestOptions = {
                method: 'POST',
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify(
                    {                
                    
                        "userId": {
                        "placemark": {
                        
                            "id": `${cellId}`
                        
                        },
                            "userId": `${userId}` 
                        }
                    
                    }
                )
               
                                
            };
            
            fetch('http://localhost:8080/v1/api/users', requestOptions)
                .then(response => {               
                    if (response.ok) {
                        console.log('Todo bien');
                      } else {
                        console.log('Respuesta de red OK pero respuesta de HTTP no OK');
                        console.log(response.status);
                        if(response.status == 405){
                            setMsgResult("Los datos ya existen")
                        }
                      }
                })         
                .catch(() => console.log("Algo falló"))
        }
       
    }


    console.log(msgResult);

/*
 {
                "placeMarkId": "L3902351",
                "userId": "MAR01"
              }   
*/
 //  .then(data => ({ placeMarkId: cellId } ))
           // .then(data => ({ userId: userId  }) )
    // response.json();
      // headers: { 'Content-Type': 'application/json' },
     // body:{ title: 'INSERT USUARIO CELLID' }
    // .then(data => this.setState({ placeMarkId: cellId }  ))
    // .then(data => this.setState({ userId: userId  }  ))

    // console.log(e);


return(
   <div className=" flex justify-center content-center ">
     <form onSubmit={enviarDatos} className="bg-white shadow-md rounded px-8 pt-6 pb-8 mb-4">
        <div className="flex flex-wrap -mx-3 mb-6">            
                
                <div className="w-full px-3" role="alert">
                            <strong className="text-center  text-red-700  font-bold">{msgResult}</strong>
                </div>
            
                <div className="w-full px-3">
                    <label className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="grid-first-name">
                        Usuario
                    </label>                    
                        <input id="usuarioID" value={userId} onChange={addUserID} type="text" placeholder=""
                         class="appearance-none block w-full bg-gray-200 text-gray-700 border  rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white" />
                          
                </div>
                
                <div className="w-full px-3">
                    <label className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="grid-password">
                        CELLID
                    </label>
                    <input value={cellId} id="cellId"  onChange={addCellID} type="text" placeholder="CELLID"
                    className="appearance-none block w-full bg-gray-200 text-gray-700 border  rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white" />                
                
                </div>
                <div className="w-full px-3">                   
                    <label className="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300" for="file_input">Upload file TXT</label>
                    <input className="block w-full text-sm text-gray-900 bg-gray-50 rounded-lg border border-gray-300 cursor-pointer dark:text-gray-400 focus:outline-none dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400" aria-describedby="file_input_help" id="file_input" type="file"/>
                    
                </div>
                
                <div className="  flex justify-center content-center w-full  py-2">
                    <button type="submit" className="flex-shrink-0 bg-teal-500 hover:bg-teal-700 border-teal-500 hover:border-teal-700 text-sm border-4 text-white py-1 px-2 rounded" >
                        Guardar
                    </button>
                    <button className="flex-shrink-0 bg-teal-500 hover:bg-teal-700 border-teal-500 hover:border-teal-700 text-sm border-4 text-white py-1 px-2 rounded" type="button">
                        Cancelar
                    </button>
                </div>
               
        </div>
        
    </form>
   </div>

    
  );
}

export default AddUser;
/*

*/
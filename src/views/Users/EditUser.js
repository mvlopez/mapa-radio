import React, { useState } from "react";


function EditUser(){

return(
    <div className=" flex justify-center content-center ">
    <form className="bg-white shadow-md rounded px-8 pt-6 pb-8 mb-4">
       <div class="flex flex-wrap -mx-3 mb-6">

           <div class="flex flex-wrap -mx-3 mb-6">
               <div class="w-full px-3">
                   <label className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="grid-first-name">
                       Usuario
                   </label>                    
                       <input class="appearance-none block w-full bg-gray-200 text-gray-700 border  rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white" id="grid-first-name" type="text" placeholder=""/>
                         
               </div>
               
               <div class="w-full px-3">
                   <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="grid-password">
                       CELLID
                   </label>
                   <input className="appearance-none block w-full bg-gray-200 text-gray-700 border  rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white" id="grid-first-name" type="text" placeholder="CELLID"/>                
               
               </div>              
               
               <div class="  flex justify-center content-center w-full  py-2">
                   <button className="flex-shrink-0 bg-teal-500 hover:bg-teal-700 border-teal-500 hover:border-teal-700 text-sm border-4 text-white py-1 px-2 rounded" type="button">
                       Guardar
                   </button>
                   <button className="flex-shrink-0 bg-teal-500 hover:bg-teal-700 border-teal-500 hover:border-teal-700 text-sm border-4 text-white py-1 px-2 rounded" type="button">
                       Cancelar
                   </button>
               </div>
             </div>  
       </div>
       
   </form>
  </div>

    
  );
}

export default EditUser;
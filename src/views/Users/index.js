import React, {
  useEffect,
  useState,
  useMemo,
  useCallback,
  useRef,
} from "react";
import DataTable from "react-data-table-component";
import differenceBy from "lodash/differenceBy";
import isEqual from "lodash/isEqual";
import { Spinner, Modal, Button } from "flowbite-react";

import { FilterComponent } from "../../components/FilterComponent";
import { AddForm } from "./AddForm";

const { REACT_APP_BASE_URL, REACT_APP_USER_PATH, REACT_APP_USER_ADD_PATH } =
  process.env;
const APIUrl = `${REACT_APP_BASE_URL}${REACT_APP_USER_PATH}`;
const APIUrlDEl = `${REACT_APP_BASE_URL}${REACT_APP_USER_ADD_PATH}`;
export default function Users() {
  const addFormRef = useRef();
  const [data, setData] = useState([]);
  const [loading, setLoading] = useState(false);
  const [totalRows, setTotalRows] = useState(0);
  const [perPage, setPerPage] = useState(10);
  const [selectedRows, setSelectedRows] = useState([]);
  const [toggleCleared, setToggleCleared] = useState(false);
  const [filterText, setFilterText] = useState("");
  const [resetPaginationToggle, setResetPaginationToggle] = useState(false);
  const [openConfirmDialog, setOpenConfirmDialog] = useState(false);
  const [openUserDialog, setOpenUserDialog] = useState(false);
  const [lastUrl, setLastUrl] = useState("");
  const [criteria, setCriteria] = useState("");

  useEffect(() => {
    fetchUsers({ criteria });
  }, [criteria]);

  // useEffect(() => {
  //   console.log({ selectedRows });
  // }, [selectedRows]);

  const fetchUsers = ({ page = 1, limit = 10, refresh = false } = {}) => {
    setLoading(true);
    const offset = page - 1; //* limit;
    var url = `${APIUrl}${criteria}?offset=${offset}&limit=${limit}&delay=${Date.now()}`;

    if (!refresh) {
      if (!criteria) {
        setLoading(false);
        return;
      }
      setLastUrl(url);
    } else {
      url = `${APIUrl}${criteria}?offset=0&limit=${limit}&delay=${Date.now()}`;
      // url = lastUrl;
    }
    // console.log({url});
    fetch(url, {
      headers: { "Content-Type": "application/json" },
    })
      .then((response) => {
        response.json().then((data) => {
          setData(data.content || []);

          setTotalRows(data.totalElements || 0);
          setLoading(false);
        });
      })
      .catch((e) => console.error(e))
      .finally(() => {
        setLoading(false);
      });
  };

  const deleteCell = ({ comercialZone, userId }) => {
    const options = {
      method: "DELETE",
      headers: { "Content-Type": "application/json" },
      body: {},
    };
    const deleteURL = `${APIUrlDEl}${userId}/${comercialZone}`;
    //console.log("delete zpna " + deleteURL);
    return fetch(deleteURL, options);
  };

  const handlePageChange = (page) => {
    fetchUsers({ page });
  };

  const handlePerRowsChange = async (newPerPage, page) => {
    setPerPage(newPerPage);
    fetchUsers({ page, limit: newPerPage });
  };

  const handleCloseConfirmDialog = () => {
    setOpenConfirmDialog(false);
  };
  const handleClickConfirmDialog = () => {
    setOpenConfirmDialog(true);
  };
  const handleUserModal = (value) => {
    setOpenUserDialog(value);
    if (value === false) {
      addFormRef.current.cleanForm();
    }
  };

  const handleDeleteConfirmDialog = () => {
    const [
      {
        userId: {
          placemark: { comercialZone = null },
          userId,
        },
      },
    ] = selectedRows;

    deleteCell({ userId, comercialZone })
      .then(() => {
        setToggleCleared(!toggleCleared);
        fetchUsers({ refresh: true });
        /*const [{userId}] = selectedRows;
        const newData =     differenceBy(
          data,
          [
            {
              userId: {
                userId: userId.userId,
                placemark: {
                  comercialZone: userId.placemark.comercialZone,
                },
              },
            },
          ],
          isEqual
        );

        console.log({newData});

        setData(
          newData
        );*/
      })
      .finally(() => {
        handleCloseConfirmDialog();
      });
  };

  // const searchTextMinus = useState("");
  /*http://localhost:8080/v1/api/users/MAR01*/
  /* http://jsonplaceholder.typicode.com/users */

  const handleRowSelected = useCallback((state) => {
    setSelectedRows(state.selectedRows);
  }, []);

  const ButtonAddCells = () => (
    <button
      type="button"
      onClick={() => handleUserModal(true)}
      className="text-white bg-sky-600/90 hover:bg-sky-700focus:ring-4 focus:outline-none focus:ring-[#3b5998]/50 font-medium rounded-lg text-sm px-5 py-2.5 text-center inline-flex items-center dark:focus:ring-[#3b5998]/55 mr-2 mb-2"
    >
      <svg
        xmlns="http://www.w3.org/2000/svg"
        fill="currentColor"
        className="bi bi-plus stroke-1 mr-2 -ml-1 w-6 h-6"
        viewBox="0 0 16 16"
      >
        {" "}
        <path d="M8 4a.5.5 0 0 1 .5.5v3h3a.5.5 0 0 1 0 1h-3v3a.5.5 0 0 1-1 0v-3h-3a.5.5 0 0 1 0-1h3v-3A.5.5 0 0 1 8 4z" />{" "}
      </svg>
      Agregar Zona Comercial
    </button>
  );

  const actionsMemo = React.useMemo(() => <ButtonAddCells />, []);

  const contextActions = useMemo(() => {
    return (
      <div className="grid grid-cols-1 gap-2">
        <button
          key="delete"
          onClick={handleClickConfirmDialog}
          className="rounded text-white bg-red-600/100 px-5 py-1 hover:bg-red-700"
        >
          Eliminar
        </button>
      </div>
    );
  }, [data, selectedRows, toggleCleared]);

  const columns = useMemo(
    () => [
      {
        name: "Zona comercial",
        selector: ({
          userId: {
            placemark: { comercialZone },
          },
        }) => comercialZone,
        sortable: true,
        /*  cell: (row) => (
          <div className="flex flex-row gap-1 items-center">
          
            {row.userId.placemark.comercialZone}
          </div>
        )*/
      },
      {
        name: "Cell ID",
        cell: (row) => (
          <div className="flex flex-row gap-1 items-center">
            {row.userId.placemark.id}
          </div>
        ),

        sortable: true,
      },
      /*{
        name: "Color",
        sortable: true,
        cell: (row) => (
          <div className="flex flex-row gap-1 items-center">
            <div
              className="w-6 h-6 rounded-full border-dashed border border-indigo-600 justify-center content-center flex items-center"
              style={{
                backgroundColor: "#" + row.userId.placemark.color,
              }}
            />
            #{row.userId.placemark.color}
          </div>
        ),
      },*/
      /*  {
        name: "Name",
        selector: ({
          userId: {
            placemark: { name },
          },
        }) => name,
        sortable: true,
        grow: 2,
      },*/
      {
        name: "Polycolor",
        cell: (row) => (
          <div className="flex flex-row gap-1 x`s-center">
            <div
              className="w-6 h-6 rounded-full border-dashed border border-indigo-600 justify-center content-center flex items-center"
              style={{
                backgroundColor: "#" + row.userId.placemark.polyColor,
              }}
            />
            #{row.userId.placemark.polyColor}
          </div>
        ),
        sortable: true,
      },
      {
        name: "User ID",
        selector: ({ userId: { userId } }) => userId,
        sortable: true,
      },
    ],
    []
  );

  const subHeaderComponentMemo = useMemo(() => {
    const handleClear = () => {
      if (filterText) {
        setResetPaginationToggle(!resetPaginationToggle);
        setFilterText("");
        setData([]);
      }
    };
    const handleSearchClick = () => {
      if (filterText) {
        const searchText = filterText.toUpperCase();
        setCriteria(searchText);
      }
    };

    return (
      <div className="flex flex-1 -ml-2 sm:flex-none sm:w-1/2 md:w-1/3 lg:1/4">
        <FilterComponent
          onFilter={(e) => setFilterText(e.target.value)}
          onClear={handleClear}
          filterText={filterText}
          onSearch={handleSearchClick}
        />
      </div>
    );
  }, [filterText, resetPaginationToggle]);
  /*
 useEffect(() => {
   const getUsers =async() =>{
            fetch("http://localhost:8080/v1/api/users/${searchTextMinus}")
            .then(Response => Response.json())
            .then(data => {  
                 setData(data);          
            });          
         };
         getUsers().catch(null);
},[]);

const [userData, setUserData] = useState(data);
*/
  /* if(data?.length){
    console.log(data);

    const filteredData =  data.filter((value) => {
        
         return (
            value.userId.userId.toLowerCase().includes(searchTextMinus) ||
            value.userId.placemark.id.toLowerCase().includes(searchTextMinus)   ||
            value.userId.placemark.name.toLowerCase().includes(searchTextMinus)
          ) 
     });

     
   //  console.log(filteredData)
     setResults(filteredData); 
            
 }*/

  //console.log(results);
  const handleChange = (state) => {
    // You can use setState or dispatch with something like Redux so we can use the retrieved data
    console.log("Selected Rows: ", state.selectedRows);
  };
  return (
    <>
      <div className="my-4 mx-4">
        <DataTable
          title="Usuario - Zona Comercial"
          columns={columns}
          data={data}
          progressPending={loading}
          progressComponent={<Spinner aria-label="Loading..." />}
          onSelectedRowsChange={handleRowSelected}
          // onRowClicked={handleChange}
          contextActions={contextActions}
          actions={actionsMemo}
          clearSelectedRows={toggleCleared}
          noDataComponent="No hay registros para mostrar"
          pagination
          paginationServer
          paginationTotalRows={totalRows}
          paginationResetDefaultPage={resetPaginationToggle} // optionally, a hook to reset pagination to page 1
          subHeader
          subHeaderComponent={subHeaderComponentMemo}
          onChangeRowsPerPage={handlePerRowsChange}
          onChangePage={handlePageChange}
          highlightOnHover
          pointerOnHover
          responsive
          selectableRows
          selectableRowsHighlight
          selectableRowsNoSelectAll
          selectableRowsSingle
          striped
        />
        {/* Confirm dialog*/}
        <Modal
          show={openConfirmDialog}
          size="md"
          popup={true}
          onClose={handleCloseConfirmDialog}
        >
          <Modal.Header />
          <Modal.Body>
            <div className="text-center">
              <h3 className="mb-5 text-lg font-normal text-gray-500 dark:text-gray-400">
                Esta Seguro de eliminar la zona comercial?
              </h3>
              <div className="flex justify-center gap-4">
                <Button color="failure" onClick={handleDeleteConfirmDialog}>
                  Si
                </Button>
                <Button color="gray" onClick={handleCloseConfirmDialog}>
                  No, Cancelar
                </Button>
              </div>
            </div>
          </Modal.Body>
        </Modal>
        {/* User form */}
        <Modal
          show={openUserDialog}
          size="md"
          popup={true}
          onClose={() => handleUserModal(false)}
        >
          <Modal.Header />
          <Modal.Body>
            <AddForm
              ref={addFormRef}
              onSave={() => {
                fetchUsers({ refresh: true });
              }}
            />
          </Modal.Body>
        </Modal>
      </div>
    </>
  );
}

import React, {useEffect,  useState } from 'react';
import { Helmet } from "react-helmet"
import { ReactSession } from 'react-client-session';


import './App.css';
//import Navbar from './components/NavBar';
import { BrowserRouter as Router,  Route,  Routes, useParams } from "react-router-dom";
//import Home from './views/Home';
import Map from './views/Map';
import MapOpen from './views/Map/MapOpen';
import Users from './views/Users';
import AddUser from './views/Users/AddUser';
import EditUser from './views/Users/EditUser';
//import { set } from 'immer/dist/internal';
//import {withRouter} from 'react-router';
import { useLocation } from "react-router-dom";

function App() {

  console.log("Inicio ") ;
  ReactSession.set("usuario", "MAR01");

  //let location = useLocation();
  //console.log("Location " + location.pathname);



  /*
// VanillaJS
const id_auto = window.location.search.split("=")[1];
const token = id_auto.split("&")[0] ;
const [data, setData] = useState([]);

//console.log("token " + token);
const AppName = "MAPA%20RADIO%20BASES";
const ApiAvi = `http://10.231.128.139:7003/SeguridadWS/resources/moduloseguridad/validarautorizacion/${token}/${AppName}`;
    
//getUsers(`http://10.231.128.139:7003/SeguridadWS/resources/moduloseguridad/validarautorizacion/${token}/MAPA RADIO BASES`);
console.log("URL " + ApiAvi);
const [username, setUsername] = useState("");
const [userData, setUserData] = useState(Object);

/*
useEffect(() => {
  const getUsers =async() =>{
           fetch(ApiAvi, {mode: "no-cors"})
           .then(Response => Response.json())
           .then(data => {  
                setData(data);          
           });          
        };
        getUsers().catch(null);
},[]);

    console.log(data);

/*
const getUsers = () =>{
  fetch(ApiAvi)
  .then(Response =>{
      console.log({Response});   
      return Response.json()
  } )
  .then(data => {  
       setData(data);   
           
  });          
};
*/



const adminUser = {
    usuario: "admin",
    password: "admin"
}

const [user, setUser] = useState({usuario: "", password:""});
const [error, setError] = useState("");

/*const Login = details => {
  console.log(details.usuario);

    if (details.usuario == adminUser.usuario  && details.password == adminUser.password){
      console.log("Logged in");

        setUser({
          usuario: details.usuario,
          
        });

    }else{
       console.log("DEtail no math");
       setError("Los datos no coinciden");
    }

}
/*
const Logout = () => {
  console.log("Logout");
  setUser({usuario: "" })
}
*/
// <Navbar/>
//"compile:production": "npm run build:production && cd build && jar -cvf mapa_radio.war ."
  return (
    <>
   <Authorize.Provider value={{ user }}>
    <BrowserRouter basename={process.env.PUBLIC_URL}>
      
      <Routes>
        <Route
          path="/"
          element={authorized ? <Home /> : <Unauthorized />}
        ></Route>
        <Route
          path="/Map"
          element={authorized ? <Map /> : <Unauthorized />}
        ></Route>
        <Route
          path="/Users/AddUser"
          element={authorized ? <AddUser /> : <Unauthorized />}
        ></Route>
        <Route
          path="/Users"
          element={authorized ? <Users /> : <Unauthorized />}
        ></Route>
      </Routes>
    </BrowserRouter>
  </Authorize.Provider>
      
local

  <BrowserRouter basename={process.env.PUBLIC_URL}>
      
      <Routes>
        <Route
          path="/"
          element={ <Home />}
        ></Route>
        <Route
          path="/Map"
          element={ <Map /> }
        ></Route>
        <Route
          path="/Users/AddUser"
          element= {<AddUser />}
        ></Route>
        <Route
          path="/Users"
          element={<Users />}
        ></Route>
      </Routes>
    </BrowserRouter>



      <Router>
       

         <Routes>          
          <Route  path="/Users/AddUser" element={<AddUser />}></Route>
        
          <Route  path="/Users" element={<Users />}></Route>
        
          <Route  path="/Map" element={<Map/>}></Route>

          </Routes>


        <Routes>          
          <Route  path="/Users/AddUser" element={<AddUser />}></Route>
          <Route  path="/Users/EditUser" element={<EditUser />}></Route>
        
          <Route  path="/Users" element={<Users />}></Route>
        
          <Route  path="/MapOpen" element={<MapOpen />}></Route>

          </Routes>
      </Router>
    
     
    </>

  );
}

/*
  <Route  path="/Map" element={<Map />}></Route>
<Route exact path="/" element={<Map />}></Route>
 {(user.usuario !="")  ? (
   
      <Router>
        <Navbar/>
        <Routes>
          <Route exact path="/" element={<Home />}></Route>
          <Route  path="/Map" element={<Map />}></Route>
          <Route  path="/Users" element={<Users />}></Route>
          <Route  path="/Users/AddUser" element={<AddUser />}></Route>
          <Route  path="/Users/EditUser" element={<EditUser />}></Route>
        </Routes>
      </Router>
      


   ): (
      <LoginForm Login={Login} error={error} />
   )
  
  }
    
*/
/*
    <div className="mx-auto mt-8">
      
        <h1 className='text-4xl text-center'> <Routes /></h1>
        <div><Sidebar /> </div>
          
    </div> */
export default App;



import React, { useEffect, useState, useContext } from "react";

import "./App.css";
//import Navbar from './components/NavBar';
import { BrowserRouter, Route, Routes } from "react-router-dom";
import Home from "./views/Home";
import Map from "./views/Map";
//import MapOpen from './views/Map/MapOpen';
import Users from "./views/Users";
import AddUser from "./views/Users/AddUser";
//import { set } from 'immer/dist/internal';
import { Unauthorized } from "./views/Home/Unauthorized";
import { Authorize } from "./context";

function App() {
  const queryParams = new URLSearchParams(window.location.search);
  const token = queryParams.get("autoriza") || null;
  const country = queryParams.get("pais") || "503";
  const appId = queryParams.get("app") || 0;

  const [msgError, setMsgError] = useState("");

  const [username, setUsername] = useState("");
  const [wsData, setWsData] = useState([]);
  const [data, setData] = useState([]);
  const [results, setResults] = useState([]);
  const [authorized, setAuthorized] = useState(false);
  const [user, setUser] = useState(null);
  const auth = useContext(Authorize);

  //ARMADO DE URL DE WS PARA CAPTURAR EL USUARIO
  const {
    REACT_APP_BASE_URL_AVI,
    REACT_APP_PATH_AVI,
    REACT_APP_NAME_APP,
    REACT_APP_IS_DEV,
  } = process.env;

  const validateUser = () => {
    const url = `${REACT_APP_BASE_URL_AVI}${REACT_APP_PATH_AVI}${token}/${REACT_APP_NAME_APP}`;
    
    // const tempResponse = {
    //   cod_error: 0,
    //   registros: 1,
    //   estado: "ok",
    //   descripcionerror: "ok",
    //   fecha: "20220820105237",
    //   retorno: {
    //     secusuarioid: 24173,
    //     usuario: "rb.fs.user3",
    //     nombre: "yo",
    //     apellido: "tu",
    //     clave: "28b7ef6a4e4113e6ba0b3ffd3237160c",
    //     email: "vladimir.rodriguez@telefonica.com",
    //     expira: 0,
    //     bloqueado: 0,
    //     intentos_fallidos: 0,
    //     fecha_cambio_clave: "Mar 4, 2022 8:51:34 PM",
    //     creado_el: "Mar 8, 2021 5:29:23 PM",
    //     creado_por: "MET02546",
    //     modificado_el: "Mar 4, 2022 8:55:49 PM",
    //     modificado_por: "RB.FS.USER3",
    //     estado: "ALTA",
    //     usuario_ldap: 0,
    //   },
    // };
    // const {
    //   cod_error,
    //   estado,
    //   retorno: { secusuarioid, usuario, nombre, apellido },
    // } = tempResponse;
    // if (cod_error == 0 && estado.trim().toLowerCase() == "ok") {
    //   setUser({ secusuarioid, usuario, nombre, apellido });
    //   setAuthorized(true);
    // }
    if (REACT_APP_IS_DEV) {
      setAuthorized(true);
      return;
    }
    console.log("URL " + url);
    fetch(url)
      .then((response) => {
        response.json().then((data) => {
          const {
            cod_error,
            estado,
            retorno: { secusuarioid, usuario, nombre, apellido },
          } = data;
          if (cod_error == 0 && estado.trim().toLowerCase() == "ok") {
            setUser({ secusuarioid, usuario, nombre, apellido });
            setAuthorized(true);
          }
        });
      })
      .catch((e) => {
        setAuthorized(false);
        console.error(e);
      });
  };
  const { user: infoUser = null } = useContext(Authorize);

  useEffect(() => {
    if (!infoUser || infoUser == null) {
      validateUser();
    } else {
      setUser(infoUser);
    }
  }, []);
  //console.log("error " + quantityNum);
  return (
    <Authorize.Provider value={{ user }}>
      <BrowserRouter basename={process.env.PUBLIC_URL}>
        <Routes>
          <Route
            path="/"
            element={authorized ? <Home /> : <Unauthorized />}
          ></Route>
          <Route
            path="/Map"
            element={authorized ? <Map /> : <Unauthorized />}
          ></Route>
          <Route
            path="/Users/AddUser"
            element={authorized ? <AddUser /> : <Unauthorized />}
          ></Route>
          <Route
            path="/Users"
            element={authorized ? <Users /> : <Unauthorized />}
          ></Route>
        </Routes>
      </BrowserRouter>
    </Authorize.Provider>
  );
  /*const Login = details => {
  console.log(details.usuario);

    if (details.usuario == adminUser.usuario  && details.password == adminUser.password){
      console.log("Logged in");

        setUser({
          usuario: details.usuario,
          
        });

    }else{
       console.log("DEtail no math");
       setError("Los datos no coinciden");
    }

}
*/
}

export default App;


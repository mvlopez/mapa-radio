import * as React from "react";
// import { Outlet, Link } from "react-router-dom";
import { BrowserRouter, Routes, Route } from "react-router-dom";

import Home from "../views/Home";
import Map from "../views/Map";
import Users from "../views/Users";

export default function MyRoutes() {
  return (
    <BrowserRouter>
      <Routes>
        <Route exact path="/" element={<Home />}></Route>
        <Route exact path="/" element={<Map />}></Route>
        <Route exact path="/" element={<Users />}></Route>
      </Routes>
    </BrowserRouter>
  );
}

//import { NativeRouter, Route, Link } from "react-router-native";
/*

import Home from   '../views/Home';
import Users from   '../views/Users';
import Map from   '../views/Map';


const RoutesComponent = () => (
    <BrowserRouter>
        <Routes>
            <Route path="/" element={<Home />} />
            <Route path="/Map" element={<Users />} />
            <Route path="/Users" element={<Map />} />
        </Routes>
  </BrowserRouter>
);*/
